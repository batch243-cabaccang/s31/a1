const server = require("http");
const url = require('url');
const port = 8000;

server
  .createServer((req, res) => {
    if (req.url == "/login") {
      res.writeHead(200, { "Content-Type": "text/plain" });
      res.end("This is the Login Page.");
      return;
    }
    res.writeHead(400, { "Content-Type": "text/plain" });
    res.end("Naliligaw ka po ba?.");
  })
  .listen(port);
console.log("Natakbo na po ang iyong server, ma'am/sir.");
